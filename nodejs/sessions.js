var db = require("./mysql_local");
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var sessions = require("client-sessions");


//allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);
app.use(sessions({
  cookieName: 'session', // cookie name dictates the key name added to the request object 
  secret: 'blargadeeblargblarg', // should be a large unguessable string 
  duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms 
  activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds 
}));


var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();               

router.route('/login').post(function(req, res) {
  req.session.user = req.body.user;
  res.json("Hello World");  
});

router.route('/who').get(function(req, res) {    
  res.json({ message: 'User is: ' + req.session.user });  
});

app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Starting server on port ' + port);